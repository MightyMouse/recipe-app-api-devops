#!bin/bash

sudo yum update -y
sudo amazon-linux-extra install -y docker
sudo systemctl enable docker.service
sudo systemctl start docker.service
sudo usermod -aG docker ec2-user
